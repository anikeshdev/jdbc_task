import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String args[]) {
        Connection c = null;
        List<User> users = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/spring_assign1",
                            "postgres", "root");
            PreparedStatement statement = c.prepareStatement("SELECT tb_user.id user_id, email, name, tb_post.id post_id, password, post FROM tb_user LEFT OUTER JOIN tb_post ON tb_user.id = tb_post.user_id;");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                User user = new User();
                user.id = result.getLong("user_id");
                user.email = result.getString("email");
                user.name = result.getString("name");
                user.password = result.getString("password");

                Post post = new Post();
                post.id = result.getLong("post_id");
                post.post = result.getString("post");
                post.userId = result.getLong("user_id");

                if (post.post != null) {
                    user.posts.add(post);
                }

                boolean isExist = false;
                for (User u: users) {
                    if (u.id == user.id) {
                        u.posts.add(post);
                        isExist = true;
                    }
                }
                if (!isExist) {
                    users.add(user);
                }
            }

            for (User user: users) {
                System.out.println(user);
            }
            result.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}

class User{
    public long id;
    public String name;
    public String email;
    public String password;
    public List<Post> posts = new ArrayList();

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", posts=" + posts +
                '}';
    }
}

class Post {
    public Long id;
    public String post;
    public Long userId;

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", post='" + post + '\'' +
                ", userId=" + userId +
                '}';
    }
}

